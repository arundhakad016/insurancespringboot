package com.insurance.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UsersPolicy;
import com.insurance.model.dto.SoldPolicyStatus;
import com.insurance.service.AdminService;


@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;

	public AdminController(AdminService adminService) {
		super();
		this.adminService = adminService;
	}
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers()
	{
		return adminService.getAllUsers();
	}
	
	@GetMapping("/getAllPolicies")
	public List<Policy> getAllPolicies()
	{
		return adminService.getAllPolicies();
	}
	
	@PostMapping("/addPolicy")
	public Policy addPolicy(@RequestBody Policy policy)
	{
		return adminService.addPolicy(policy);
	}
	
	@PutMapping("/updatePolicy")
	public Policy updatePolicy(@RequestBody Policy policy)
	{
		return adminService.updatePolicy(policy);
		
	}
	
	@DeleteMapping("/deletePolicy/{id}")
	public String deletePolicy(@PathVariable("id") int id)
	{
		
		return adminService.deletePolicyById(id);	
		
	}
	
	@PostMapping("/addUnderwriter")
	public User addUnderwriter(@RequestBody User user)
	{
		user.setRoles(new Role("ROLE_UNDERWRITER"));
		return adminService.save(user);
		
	}
	
	
	@PutMapping("/updateUnderwriter")
	public User updateUnderwriter(@RequestBody User user)
	{
		return adminService.updateUnderwriter(user);
		
	}
	
	@DeleteMapping("/deleteUnderwriter/{id}")
	public String deleteUnderwriter(@PathVariable("id") int id)
	{
		
		return adminService.deleteUnderwriterById(id);	
		
	}
	
	@GetMapping("/getUnderwriters")
	public List<User> getAllUnderwriters()
	{
		return adminService.getAllUnderwriters();
	}
	@GetMapping("/getPurchasedPolicy")
	public List<UsersPolicy> getPurchasedPolicy()
	{
		return adminService.getPurchasedPolicy();
	}
	
	@GetMapping("/getUsersPolicyByPolicyId/{policyId}")
	public boolean getUsersPolicyByPolicyId(@PathVariable("policyId") int policyId)
	{
		System.out.println("****************************************");
		return adminService.getUsersPolicyByPolicy(policyId);
	}
	
	
	@GetMapping("/getAllPoliciesWithSellStatus")
	public List<SoldPolicyStatus> policyWithSellStatus()
	{
		
		return adminService.policyWithSellStatus();
	}
	
}
