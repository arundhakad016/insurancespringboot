package com.insurance.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.insurance.model.Claim;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.service.AdminService;
import com.insurance.service.UserService;

import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {
	

	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	
	

	public UserController(AdminService adminService) {
		super();
		this.adminService = adminService;
	}
	
	
	@PostMapping(value = "/register")
	public User registerUser(@RequestBody User user)
	{
		user.setRoles(new Role("ROLE_USER"));
		return adminService.save(user);		
	}
	
	
	@GetMapping("/getUserByUsername/{username}")
	public User getUserByUsername(@PathVariable("username") String username)
	{	
		
		return adminService.getUserByUsername(username);	
		
	}
	
	
	@PostMapping("/addDependents")
    public List<UserDependents> addDependent(@RequestBody List<UserDependents> userDependents)
    {
        return userService.addDependents(userDependents);
       
    }
	
	
	@GetMapping("/getAllDependents")
	public List<UserDependents> getAllDependents()
	{	
		
		return this.userService.getAllDependents(); 
		
	}
	
	@PutMapping("/updateDependent")
	public UserDependents updateDependent(@RequestBody UserDependents dependent)
	{
		return userService.updateDependent(dependent);
	}
	
	@PutMapping("/updateUserProfile")
	public User updateUserProfile(@RequestBody User user)
	{
		return userService.updateUserProfile(user);
	}
	
	@GetMapping("/getPoliciesByCategory/{cat}")
	public List<Policy> getPoliciesByCategory(@PathVariable("cat") String cat)
	{
		return userService.getPoliciesByCategory(cat);
	}
	
    @GetMapping("/getDependentsByPolicyMaxAge/{policyMaxAge}")
    public List<UserDependents> getDependentsByPolicyMaxAge(@PathVariable("policyMaxAge") int policyMaxAge)
    {
        return userService.getDependentsByPolicyMaxAge(policyMaxAge);
    }
    
    @PostMapping("/purchasePolicy")
    public UsersPolicy purchasePolicy(@RequestBody UsersPolicy usersPolicy)
    {
    	return userService.purchasePolicy(usersPolicy);
    }
    
 
    
    @GetMapping("/getUserClaimsByUserPolicy/{userPolicyId}")
	public List<Claim> getUserClaimsByUserPolicy(@PathVariable("userPolicyId") int usersPolicyId)
	{
		return userService.getUserClaimsByUserPolicy(usersPolicyId);
	}
    
    @PostMapping("/addClaim")
    public Claim addClaim(@RequestBody Claim claim)
    {
    	return userService.addClaim(claim);
    }
    

    
    @PutMapping("/updateUsersPolicy")
    public UsersPolicy updateUsersPolicy(@RequestBody UsersPolicy usersPolicy)
    {
    	return userService.updateUsersPolicy(usersPolicy);
    }
    
   
    
    @PostMapping("/addNominee") 
    public Nominee addNominee(@RequestBody Nominee nominee)
    {
    	return this.userService.addNominee(nominee);
    }
    
    @GetMapping("/getUsersPolicies")
    public List<UsersPolicy> getUserPolicies()
    {
        return userService.getAllUserPolicies();
    }
    
    
    @GetMapping("/getUsersPoliciesByStatus/{status}")
    public List<UsersPolicy> getUserPoliciesByStatus(@PathVariable("status") String status)
    {
        return userService.getUserPoliciesByStatus(status);
    }
    
	/*@RequestMapping("/signup")
	public String signup(Model model)
	{
		model.addAttribute("user", new User());
		return "signup";
	}
	
	@PostMapping(value = "/do_register")
	public String registerUser(@ModelAttribute("user") User user)
	{
		user.setRoles(new Role("ROLE_USER"));
		adminService.save(user);
		
		return "login";
	}*/


}
