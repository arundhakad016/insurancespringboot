package com.insurance.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.insurance.model.enums.Gender;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String email;
	
	private String password;
	
	private String phoneNumber;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	private String address;
	
	private Date dob;
	
	@Column(name = "medical_condition")
	private boolean medicalCondition;
	
	@Column(name = "medical_condition_desc")
	private String medicalConditionDesc;
	
	@ManyToOne
	@JoinColumn(name = "roles")
	private Role role;

	public User() {
		super();
	}

	public User(int id, String name, String email, String password, String phoneNumber, Gender gender, String address,
			Date dob, boolean medicalCondition, String medicalConditionDesc, Role role) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.gender = gender;
		this.address = address;
		this.dob = dob;
		this.medicalCondition = medicalCondition;
		this.medicalConditionDesc = medicalConditionDesc;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob=dob;
	}

	public boolean isMedicalCondition() {
		return medicalCondition;
	}

	public void setMedicalCondition(boolean medicalCondition) {
		this.medicalCondition = medicalCondition;
	}

	public String getMedicalConditionDesc() {
		return medicalConditionDesc;
	}

	public void setMedicalConditionDesc(String medicalConditionDesc) {
		this.medicalConditionDesc = medicalConditionDesc;
	}

	public Role getRoles() {
		return role;
	}

	public void setRoles(Role role) {
		this.role = role;
	}
	
	
}
