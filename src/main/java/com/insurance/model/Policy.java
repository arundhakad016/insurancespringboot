package com.insurance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import com.insurance.model.enums.Category;

@Entity
@Table(name = "policies")
public class Policy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String description;
	
	@Enumerated(EnumType.STRING)
	private Category category;
	
	@Column(name = "marked_premium")
	private int markedPremium;
	
	private int coverage;
	
	private int tenure;
	
	@Column(name = "max_age")
	private int maxAge;

	public Policy() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Policy(int id, String name, String description, Category category, int markedPremium, int coverage,
			int tenure, int maxAge) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.category = category;
		this.markedPremium = markedPremium;
		this.coverage = coverage;
		this.tenure = tenure;
		this.maxAge = maxAge;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public int getMarkedPremium() {
		return markedPremium;
	}

	public void setMarkedPremium(int markedPremium) {
		this.markedPremium = markedPremium;
	}

	public int getCoverage() {
		return coverage;
	}

	public void setCoverage(int coverage) {
		this.coverage = coverage;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

}
