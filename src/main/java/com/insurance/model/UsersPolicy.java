package com.insurance.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.insurance.model.enums.Status;

@Entity
@Table(name = "users_policy")
public class UsersPolicy {
	

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	private int id;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name = "policy_id")
	private Policy policy;
	
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name = "sold_premium")
	private int soldPremium;	
	
	@Column(name = "purchased_date")
	private Date purchasedDate;
	
	@Column(name = "expire_date")
	private Date expireDate;
	
	@Column(name = "has_dependent")
	private boolean hasDependent;
	
	@Column(name="claimed_amount")
	private int claimedAmount;
	
	@ManyToMany
	@JoinTable
	private Collection<UserDependents> userDependents;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name="nominee_id")
	private Nominee nominee;
	

	public UsersPolicy() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UsersPolicy(int id, User user, Policy policy, Status status, int soldPremium, Date purchasedDate,
			Date expireDate, boolean hasDependent, int claimedAmount, Collection<UserDependents> userDependents,
			Nominee nominee) {
		super();
		this.id = id;
		this.user = user;
		this.policy = policy;
		this.status = status;
		this.soldPremium = soldPremium;
		this.purchasedDate = purchasedDate;
		this.expireDate = expireDate;
		this.hasDependent = hasDependent;
		this.claimedAmount = claimedAmount;
		this.userDependents = userDependents;
		this.nominee = nominee;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Policy getPolicy() {
		return policy;
	}


	public void setPolicy(Policy policy) {
		this.policy = policy;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public int getSoldPremium() {
		return soldPremium;
	}


	public void setSoldPremium(int soldPremium) {
		this.soldPremium = soldPremium;
	}


	public Date getPurchasedDate() {
		return purchasedDate;
	}


	public void setPurchasedDate(Date purchasedDate) {
		this.purchasedDate = purchasedDate;
	}


	public Date getExpireDate() {
		return expireDate;
	}


	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}


	public boolean isHasDependent() {
		return hasDependent;
	}


	public void setHasDependent(boolean hasDependent) {
		this.hasDependent = hasDependent;
	}


	public int getClaimedAmount() {
		return claimedAmount;
	}


	public void setClaimedAmount(int claimedAmount) {
		this.claimedAmount = claimedAmount;
	}


	public Collection<UserDependents> getUserDependents() {
		return userDependents;
	}


	public void setUserDependents(Collection<UserDependents> userDependents) {
		this.userDependents = userDependents;
	}


	public Nominee getNominee() {
		return nominee;
	}


	public void setNominee(Nominee nominee) {
		this.nominee = nominee;
	}

	

	
	
	
}
