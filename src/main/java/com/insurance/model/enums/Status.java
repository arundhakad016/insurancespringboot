package com.insurance.model.enums;

public enum Status {
	Approved,
	Pending,
	Paid,
	Completed,
	Rejected
}
