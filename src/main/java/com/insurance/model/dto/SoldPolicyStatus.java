package com.insurance.model.dto;

import com.insurance.model.Policy;

public class SoldPolicyStatus {
private Policy policy;
private boolean sold;
public SoldPolicyStatus() {
	super();
	// TODO Auto-generated constructor stub
}
public SoldPolicyStatus(Policy policy, boolean sold) {
	super();
	this.policy = policy;
	this.sold = sold;
}
public Policy getPolicy() {
	return policy;
}
public void setPolicy(Policy policy) {
	this.policy = policy;
}
public boolean getSold() {
	return sold;
}
public void setSold(boolean sold) {
	this.sold = sold;
}


}
