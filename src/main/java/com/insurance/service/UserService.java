package com.insurance.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.insurance.config.UserDetailsServiceImpl;
import com.insurance.exception.ResourceNotFoundException;
import com.insurance.model.Claim;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Status;
import com.insurance.repository.ClaimRepository;
import com.insurance.repository.DependentsRepository;
import com.insurance.repository.NomineeRepository;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PolicyRepository policyRepository;

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;

	@Autowired
	private NomineeRepository nomineeRepository;

	@Autowired
	private DependentsRepository dependentsRepository;

	@Autowired
	private ClaimRepository claimRepository;

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	public UserService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserService(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository,
			PolicyRepository policyRepository, UsersPolicyRepository usersPolicyRepository,
			NomineeRepository nomineeRepository, DependentsRepository dependentsRepository,
			ClaimRepository claimRepository) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.userRepository = userRepository;
		this.policyRepository = policyRepository;
		this.usersPolicyRepository = usersPolicyRepository;
		this.nomineeRepository = nomineeRepository;
		this.dependentsRepository = dependentsRepository;
		this.claimRepository = claimRepository;
	}

	public List<UserDependents> addDependents(List<UserDependents> dependents) {

		List<UserDependents> l = new ArrayList<UserDependents>();

		for (int i = 0; i < dependents.size(); i++)
			l.add(dependentsRepository.save(dependents.get(i)));

		return l;
	}

	public List<UserDependents> getAllDependents() {
		User user =  userRepository.findByEmail(userDetailsServiceImpl.currentName());
		return this.dependentsRepository.findByUser(user);
	}

	public UserDependents updateDependent(UserDependents dependent) {
		return this.dependentsRepository.save(dependent);
	}

	public User updateUserProfile(User user) {
		return this.userRepository.save(user);
	}

	public List<Policy> getPoliciesByCategory(String cat) {

		User currentUser = userRepository.findByEmail(userDetailsServiceImpl.currentName());
		LocalDate birthDate = currentUser.getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate currentDate = java.time.LocalDate.now();
		int age = Period.between(birthDate, currentDate).getYears();
		List<Policy> policies = policyRepository.findByCategory(Category.valueOf(cat));
		List<Policy> selectedPolicies=new ArrayList<>();
		for(Policy policy:policies)
		{
			if(policy.getMaxAge()>age) selectedPolicies.add(policy);
		}
		
		return selectedPolicies;
	}

	public int getDependentAge(UserDependents userDependent) {
		LocalDate birthDate = userDependent.getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate currentDate = java.time.LocalDate.now();
		return Period.between(birthDate, currentDate).getYears();
	}

	public List<UserDependents> getDependentsByPolicyMaxAge(int policyMaxAge) {
		User currentUser = userRepository.findByEmail(userDetailsServiceImpl.currentName());
		List<UserDependents> dependents=new ArrayList<>();
		List<UserDependents> selectedDependents=new ArrayList<>();
	    dependents = dependentsRepository.findByUser(currentUser);
		for (UserDependents dependent : dependents) {
			if (getDependentAge(dependent) < policyMaxAge) selectedDependents.add(dependent);
				
		}
		
		
		return selectedDependents;
	}
	 public UsersPolicy purchasePolicy(UsersPolicy usersPolicy)
	    {
	    	return usersPolicyRepository.save(usersPolicy);
	    }
	 
	
	 
	 
	 public List<Claim> getUserClaimsByUserPolicy(int  usersPolicyId)
		{
		 UsersPolicy usersPolicy=usersPolicyRepository.findById(usersPolicyId).get();
		 return claimRepository.findByUsersPolicy(usersPolicy);
		}
	  
	 public Claim addClaim(Claim claim)
	 {
		 return claimRepository.save(claim);
	 }
	 
	 public UsersPolicy updateUsersPolicy(UsersPolicy usersPolicy)
	 {
		 return this.usersPolicyRepository.save(usersPolicy);
	 }
	  public List<UsersPolicy> getUserPoliciesByStatus(String status)
	 {
		 return usersPolicyRepository.findByStatusAndUser(Status.valueOf(status), userRepository.findByEmail(userDetailsServiceImpl.currentName()));	  
	 }
	 public Nominee addNominee(Nominee nominee)
	 {
		 return this.nomineeRepository.save(nominee);
	 }
	 
	   public List<UsersPolicy> getAllUserPolicies() {
	        User currentUser = userRepository.findByEmail(userDetailsServiceImpl.currentName());
	 
	        return usersPolicyRepository.findByUser(currentUser);
	    }
	
}
