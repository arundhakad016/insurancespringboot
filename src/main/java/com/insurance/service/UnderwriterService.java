package com.insurance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.insurance.model.User;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Status;
import com.insurance.repository.ClaimRepository;
import com.insurance.repository.DependentsRepository;
import com.insurance.repository.NomineeRepository;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UserRepository;
import com.insurance.repository.UsersPolicyRepository;

@Service
public class UnderwriterService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PolicyRepository policyRepository;

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;

	@Autowired
	private NomineeRepository nomineeRepository;

	@Autowired
	private DependentsRepository dependentsRepository;

	@Autowired
	private ClaimRepository claimRepository;

	public UnderwriterService(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository,
			PolicyRepository policyRepository, UsersPolicyRepository usersPolicyRepository,
			NomineeRepository nomineeRepository, DependentsRepository dependentsRepository,
			ClaimRepository claimRepository) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.userRepository = userRepository;
		this.policyRepository = policyRepository;
		this.usersPolicyRepository = usersPolicyRepository;
		this.nomineeRepository = nomineeRepository;
		this.dependentsRepository = dependentsRepository;
		this.claimRepository = claimRepository;
	}
	
    public List<UsersPolicy> getUserPoliciesByStatus(String status) {
        List<UsersPolicy> pol = usersPolicyRepository.findByStatus(Status.valueOf(status));
        System.out.println(pol);
        return pol;
    }
   
    
    public UsersPolicy updateUsersPolicy(UsersPolicy usersPolicy) {
        return usersPolicyRepository.save(usersPolicy);
    }
	
}
