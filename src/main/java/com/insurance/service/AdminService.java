package com.insurance.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.insurance.exception.ResourceNotFoundException;
import com.insurance.model.Claim;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.dto.SoldPolicyStatus;
import com.insurance.repository.ClaimRepository;
import com.insurance.repository.DependentsRepository;
import com.insurance.repository.NomineeRepository;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.repository.UserRepository;

@Service
public class AdminService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PolicyRepository policyRepository;

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;

	@Autowired
	private NomineeRepository nomineeRepository;

	@Autowired
	private DependentsRepository dependentsRepository;

	@Autowired
	private ClaimRepository claimRepository;

	public AdminService(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository,
			PolicyRepository policyRepository, UsersPolicyRepository usersPolicyRepository,
			NomineeRepository nomineeRepository, DependentsRepository dependentsRepository,
			ClaimRepository claimRepository) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.userRepository = userRepository;
		this.policyRepository = policyRepository;
		this.usersPolicyRepository = usersPolicyRepository;
		this.nomineeRepository = nomineeRepository;
		this.dependentsRepository = dependentsRepository;
		this.claimRepository = claimRepository;
	}

	
	public User save(User user) {

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		return userRepository.save(user);
	}


	public List<User> getAllUsers() {
		return userRepository.findAll();
	}
	
	public User getUserByUsername(String username)
	{
		return userRepository.findByEmail(username);
	}
	
	public List<User> getAllUnderwriters() {
		return (List<User>) userRepository.findByRole(new Role("ROLE_UNDERWRITER"));
	}
	

	public Policy addPolicy(Policy policy) {
		return policyRepository.save(policy);
	}
	

	public List<Policy> getAllPolicies() {
		return policyRepository.findAll();
	}
	

	public Policy updatePolicy(Policy policy) {
		return policyRepository.save(policy);
	}
	
	public User updateUnderwriter(User user) {
		return userRepository.save(user);
	}
	
	

	public String  deletePolicyById(int id) {
		try { 
		policyRepository.deleteById(id);
		return "Policy Deleted SuccessFully with id "+id;
		}catch(ResourceNotFoundException e) {
		return "Failed to delete policy with id "+id;
	}
	}
		public String  deleteUnderwriterById(int id) {
			try { 
			userRepository.deleteById(id);
			return "Underwriter Deleted SuccessFully with id "+id;
			}catch(ResourceNotFoundException e) {
			return "Failed to delete Underwriter with id "+id;
		}
		
}
	


	public Nominee addNominee(Nominee nominee) {
		return nomineeRepository.save(nominee);
	}


	public UsersPolicy addPurchasedPolicy(UsersPolicy usersPolicy) {
		return usersPolicyRepository.save(usersPolicy);
	}


	public UserDependents addDependents(UserDependents userDependents) {
		return dependentsRepository.save(userDependents);
	}


	public Claim addClaim(Claim claim) {
		return claimRepository.save(claim);
	}
	
	public List<UsersPolicy> getPurchasedPolicy()
	{
		return usersPolicyRepository.findAll();
	}
	
	public boolean getUsersPolicyByPolicy(int policyId)
	{
		Policy policy=policyRepository.findById(policyId).get();
		
		System.out.println(usersPolicyRepository.findByPolicy(policy).size()!=0);
		return usersPolicyRepository.findByPolicy(policy).size()!=0;
	}
	
	
	public List<SoldPolicyStatus> policyWithSellStatus()
	{
		List<SoldPolicyStatus> soldPolicyStatus=new ArrayList<>();
		List<Policy> policies=new ArrayList<Policy>();
		SoldPolicyStatus soldPolicy;
		policies=policyRepository.findAll();
		for(Policy policy:policies)
		{
			soldPolicy=new SoldPolicyStatus(policy,false);
			//Will return empty list when no result found
			if(usersPolicyRepository.findByPolicy(policy).size()!=0)
			{
				soldPolicy.setSold(true);
			}
			soldPolicyStatus.add(soldPolicy);
		}
		return soldPolicyStatus;
	}
	
}
