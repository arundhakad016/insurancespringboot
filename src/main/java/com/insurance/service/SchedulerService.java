package com.insurance.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Status;
import com.insurance.repository.UsersPolicyRepository;

@Service
public class SchedulerService {
	@Autowired
    private UsersPolicyRepository usersPolicyRepository;
   
    //@Scheduled(fixedRate = 5000)
    public void checkingExpiryOfUsersPolicy()
    {
        List<UsersPolicy> list = usersPolicyRepository.findAll();
        LocalDate currentDate = java.time.LocalDate.now();
        for(int i=0;i<list.size();i++)
        {
            if((list.get(i).getExpireDate()).toString().substring(0, 10).equals((currentDate).toString().substring(0,10)))
            {
                list.get(i).setStatus(Status.valueOf("Completed"));
            }
            //System.out.println((list.get(i).getExpireDate()).toString().substring(0, 10));
        }
        System.out.println(currentDate.toString());
    }
   
   
    //@Scheduled(fixedRate = 2 * 24 * 60 * 60 * 1000)
    public void removeRejectedPolicies()
    {
        List<UsersPolicy> list = usersPolicyRepository.findAll();
        for(int i=0;i<list.size();i++)
        {
            if(list.get(i).getStatus().equals(Status.valueOf("Completed")))
            {
                usersPolicyRepository.deleteById(list.get(i).getId());
            }
        }
    }
 
}
