package com.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.insurance.model.User;
import com.insurance.model.UserDependents;

@Repository
public interface DependentsRepository extends JpaRepository<UserDependents, Integer>{
	public List<UserDependents> findByUser(User user);
}
