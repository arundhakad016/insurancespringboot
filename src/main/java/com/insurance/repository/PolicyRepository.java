package com.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.insurance.model.Policy;
import com.insurance.model.enums.Category;

@Repository
public interface PolicyRepository extends JpaRepository<Policy, Integer>{
	public List<Policy> findByCategory(Category cat);

}
